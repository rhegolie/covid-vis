import Vue from 'vue'
import App from './App.vue'
import store from './store'
Vue.config.productionTip = false

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// import './app.scss'
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

import Vuex from 'vuex'
Vue.use(Vuex)

import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css'
Vue.component('v-select', vSelect)

import VueApexCharts from 'vue-apexcharts'
Vue.use(VueApexCharts)
Vue.component('apexchart', VueApexCharts)

new Vue({
  store,
  render: h => h(App),
  components: { App }
}).$mount('#app')
